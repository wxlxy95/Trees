﻿namespace Trees
{
    public interface IStack<T>
    {
        T Pop();
        void Push(T item);
    }
}
