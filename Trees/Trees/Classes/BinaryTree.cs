﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Trees
{
    public class BinaryTree<T> : IBinaryTree<T>
    {
        private IBinaryTree<T> _leftNode;
        private IBinaryTree<T> _rightNode;

        public T Data { get; set; }

        public IBinaryTree<T> LeftNode { get => _leftNode; set => _leftNode = value; }
        public IBinaryTree<T> RightNode { get => _rightNode; set => _rightNode = value; }

        public T LeftData
        {
            get => LeftNode.Data;
            set => SetNodeData(ref _leftNode, value);
        }

        public T RightData
        {
            get => RightNode.Data;
            set => SetNodeData(ref _rightNode, value);
        }

        public bool IsEmpty => Data == null && LeftNode == null && RightNode == null;
        public bool IsFull => false;
        public IEnumerable<ITree<T>> ChildNodes
        {
            get
            {
                yield return LeftNode;
                yield return RightNode;
            }
        }

        public void Clear()
        {
            Data = default(T);
            LeftNode = null;
            RightNode = null;
        }

        public void Print()
        {
            throw new NotImplementedException();
        }

        public void Insert(T data)
        {
            throw new NotImplementedException();
        }

        public T Remove(Predicate<T> determiner)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> AsEnumerable() => throw new NotImplementedException();

        public IEnumerable<T> AsEnumerable<TEnumerator>() => new VirtualEnumerable<T, TEnumerator>();

        #region Enumerators

        public class PreOrderEnumerator : IEnumerator<T>
        {
            public T Current => throw new NotImplementedException();

            object IEnumerator.Current => throw new NotImplementedException();

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool MoveNext()
            {
                throw new NotImplementedException();
            }

            public void Reset()
            {
                throw new NotImplementedException();
            }
        }

        public class InOrderEnumerator : IEnumerator<T>
        {
            public T Current => throw new NotImplementedException();

            object IEnumerator.Current => throw new NotImplementedException();

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool MoveNext()
            {
                throw new NotImplementedException();
            }

            public void Reset()
            {
                throw new NotImplementedException();
            }
        }

        public class PostOrderEnumerator : IEnumerator<T>
        {
            public T Current => throw new NotImplementedException();

            object IEnumerator.Current => throw new NotImplementedException();

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool MoveNext()
            {
                throw new NotImplementedException();
            }

            public void Reset()
            {
                throw new NotImplementedException();
            }
        }

        #endregion // Enumerators

        #region Helper Methods

        private void SetNodeData(ref IBinaryTree<T> node, T value)
        {
            if (node == null)
            {
                node = new BinaryTree<T>();
            }
            node.Data = value;
        }

        #endregion // Helper Methods
    }
}
